/* news feed easyTicker */
$(document).ready(function() {
    $('.news-content').easyTicker({
        direction: 'up',
        easing: 'easeOutSine',
        speed: 'slow',
        interval: 5000,
        height: '500px',
        visible: 0,
        mousePause: 0,
        controls: {
            up: '.up',
            down: '.down',
            playText: 'Play'
        }
    });
});
