$(function () {
    $("#headerCarousel").carousel({
        interval: 3000,
        pause: false
    });
}); // carousel-animation header

$(function () {
    if (window.innerHeight > window.innerWidth) {
        var deviceWidth = document.documentElement.clientWidth;
        if (deviceWidth < 400) {
            $("#headerHome").remove();
            $("#top-hr").remove();
        } // portrait orientation do this
    } else {
        var deviceHeight = document.documentElement.clientHeight;
        if (deviceHeight < 400) {
            $("#headerHome").remove();
            $("#top-hr").remove();
            $("#header-desc").remove();
            $("#header-more-btn").remove();
        } // landscape orientation do this
    } // end if else: check device orientation
}); // removing the carousel text for small devices

$(function() {
    $("#featuredCarousel").carousel({
        interval: 5000,
        pause: true
    });
}); // carousel-animation feature

// jQuery to collapse the navbar on scroll
function collapseNavbar() {
    if ($(".navbar").offset().top > 50) {
        $(".navbar-fixed-top").addClass("top-nav-collapse");
    } else {
        $(".navbar-fixed-top").removeClass("top-nav-collapse");
    }
}

$(window).scroll(collapseNavbar);
$(window).ready(collapseNavbar);

// jQuery for page scrolling feature
$(function () {
    $('a.page-scroll').bind('click', function (event) {
        var $link = $(this);
        $('html, body').stop().animate({
            scrollTop: $($link.attr('href')).offset().top
        }, 1500, 'easeInOutExpo');
        event.preventDefault();
    });
});

// Closes the Responsive Menu on Menu Item Click
$('.navbar-collapse ul li a').click(function () {
    if ($(this).attr('class') != 'dropdown-toggle active' && $(this).attr('class') != 'dropdown-toggle') {
        $('.navbar-toggle:visible').click();
    }
});

/* maginfic popup gallery */
$(document).ready(function () {
    $('.popup-gallery').magnificPopup({
        delegate: 'a',
        type: 'image',
        tLoading: 'Loading image #%curr%...',
        mainClass: 'mfp-img-mobile',
        gallery: {
            enabled: true,
            navigateByImgClick: false,
            preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
        },
        image: {
            tError: '<a href="%url%">The image #%curr%</a> could not be loaded.'
                /*titleSrc: function(item) {
                	return item.el.attr('events-box') + '<small>ammwinchande</small>';
                }*/
        }
    });
});

// Page loading animation
$(window).load(function() {
    // animate loader off screen
    $(".page-loader").fadeOut("slow");
});

